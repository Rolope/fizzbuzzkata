describe("fizzBuzz", function() {
 
    it("has a list from 1 to 100", function() {
    let fizzBuzz = new FizzBuzz();
    
    let result = fizzBuzz.getArray(); 

      expect(result[1]).toBe(2)
      expect(result[43]).toBe(44)
      expect(result[97]).toBe(98)
    });

    it("if has a number divisible by 3 write 'Fizz'.", function() {
      let fizzBuzz = new FizzBuzz();
      
      let result = fizzBuzz.getArray(); 
  
        expect(result[2]).toBe('Fizz')
        expect(result[32]).toBe('Fizz')
      });
  
      it("if has a number divisible by 5 write 'Buzz'.", function() {
        let fizzBuzz = new FizzBuzz();
        
        let result = fizzBuzz.getArray(); 
    
          expect(result[84]).toBe('Buzz')
          expect(result[54]).toBe('Buzz')
        });
});
